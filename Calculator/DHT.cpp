#include "DHT_Only_Humidity.h"

#define MIN_INTERVAL	2000
#define TIMEOUT			UINT32_MAX
DHT::DHT(uint8_t count){
	(void) count;
	_maxCycles = microsecondsToClockCycles(1000);
}
void DHT::initialize(uint8_t usec){
	
	pinMode(_pinNumber,INPUT_PULLUP);
	_lastReadTime = millis() - MIN_INTERVAL;
	pullTime = usec;
}
float DHT::readHumidity(bool state){
	float hum = 0;
	if(read(state)){
		hum = (((word)busData[0] << 8) | busData[1]);
		hum *= 0.1f;
	}
	return hum;
}
bool DHT::read(bool force){
	uint32_t currentTime = millis();
	if(!force && ((currentTime - _lastReadTime)< MIN_INTERVAL))
		return _lastResult;
	_lastReadTime = currentTime;
	busData[0] = 0; busData[1] = 0;
	
	pinMode(_pinNumber,OUTPUT);
	digitalWrite(_pinNumber,LOW);
	delayMicroseconds(1100);
	uint32_t cycles[80];
	pinMode(_pinNumber, INPUT_PULLUP);
	delayMicroseconds(pullTime);
	if (findPulse(LOW) == TIMEOUT) {
		_lastResult = false;
		return _lastResult;
	}
	else if(findPulse(HIGH) == TIMEOUT){
		_lastResult = false;
		return _lastResult;
	}
	for (uint8_t i = 0; i < 80; i+=2){
		cycles[i] = findPulse(LOW);
		cycles[i + 1] = findPulse(HIGH);
	}
	for (uint8_t k = 0; k < 40; k++){
		uint32_t lowCycles = cycles[k * 2];
		uint32_t highCycles = cycles[k * 2 + 1];
		if((lowCycles == TIMEOUT) || (highCycles == TIMEOUT)){
			_lastResult = false;
			return _lastResult;
		}
		busData [k / 8] <<= 1;
		if (highCycles > lowCycles)
			busData[k / 8] |= 1;
	}
	if (busData[4] == ((busData[0] + busData[1] + busData[2] + busData[3]) & 0xFF)){
		_lastResult = true;
		return _lastResult;
	}
	else{
		_lastResult = false;
		return _lastResult;
	}
}
uint32_t DHT::findPulse(bool level){
	#if (F_CPU > 16000000L)
		uint32_t count = 0;
	#else
		uint8_t count = 0;
	#endif
	while(digitalRead(_pinNumber) == level){
		if(count++ >= _maxCycles)
			return TIMEOUT;
	}
	return count;
}
			
		
			
	
	
