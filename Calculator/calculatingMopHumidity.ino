#include "DHT_Only_Humidity.h"
#include <LiquidCrystal_I2C.h>

LiquidCrystal_I2C screen16x2(0x27,16,2);
DHT mopHumidityCalculator;
float humidityPercent;



void setup() {
  Serial.begin(115200);
  mopHumidityCalculator.initialize();
  screen16x2.init();
  screen16x2.backlight();
}

void loop() {
  humidityPercent = mopHumidityCalculator.readHumidity();
  Serial.println(humidityPercent);
  delay(500);
  screen16x2.setCursor(3,0);
  screen16x2.print("Nem %: ");
  screen16x2.print(humidityPercent);
  screen16x2.setCursor(0,1);
  screen16x2.print("ERMOP Nem Olcer");
}
