#ifndef _DHT_ONLY_HUMIDITY_H_
#define _DHT_ONLY_HUMIDITY_H_

#include <Arduino.h>

#define DHT_TYPE	AM2301
#define DHT_PIN   3

class DHT{
	public:
		DHT (uint8_t count = 1);
		void initialize(uint8_t delayUsc = 55);
		float readHumidity(bool force = false);
		bool read(bool force = false);
	
	private:
    	uint8_t _pinNumber = DHT_PIN;
		uint8_t  _count;  		// Number of sensors
		uint8_t  busData[5];
		uint32_t _lastReadTime, _maxCycles;
		bool	 _lastResult;
		uint8_t  pullTime;
		uint32_t findPulse(bool level);
};
#endif
		
	
	
